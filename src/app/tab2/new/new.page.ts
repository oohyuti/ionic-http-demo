import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-new',
  templateUrl: './new.page.html',
  styleUrls: ['./new.page.scss'],
})
export class NewPage implements OnInit {
  title;
  description;
  constructor(
    private router: Router,
    private http: HttpClient,
    private navController: NavController,
    private alertController: AlertController,
  ) {}

  ngOnInit() {}

  createContent() {
    const token = localStorage.getItem('token');
    const url = 'https://api.cocoing.info/admin/notifications';
    const body = {
      title: this.title,
      description: this.description,
    };
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.post<any>(url, body, httpOptions).subscribe(
      (response) => {
        this.presentSuccessAlert();
      },
      (error) => {
        this.presentErrorAlert();
      },
    );
  }

  async presentSuccessAlert() {
    const alert = await this.alertController.create({
      header: 'Success',
      message: '新增成功',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.router.navigate(['/tabs/tab2']);
          },
        },
      ],
    });
    alert.present();
  }

  async presentErrorAlert() {
    const alert = await this.alertController.create({
      header: 'Error',
      message: '新增失敗',
      buttons: ['OK'],
    });
    alert.present();
  }
}
