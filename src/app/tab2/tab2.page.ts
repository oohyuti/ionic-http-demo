import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page {
  data;
  allowReloadButton: boolean;
  data$ = of(null);
  tmp$ = of(null);

  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private alertController: AlertController,
    private router: Router,
  ) {
    activatedRoute.params.subscribe((val) => {
      this.data$ = this.initialize();
    });
  }

  // tslint:disable-next-line:use-lifecycle-interface
  async ngOnInit() {
    if (!localStorage.getItem('token')) {
      this.notUser();
    } else {
      this.data$ = this.initialize();
      this.allowReloadButton = true;
    }
  }

  reload() {
    this.data$ = this.initialize();
  }

  initialize() {
    const token = localStorage.getItem('token');
    const url = 'https://api.cocoing.info/admin/notifications';
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.get<any>(url, httpOptions).pipe(
      map((response) => {
        this.checkUser(response.data);
        return response.data;
      }),
      catchError(this.handleError),
    );
  }

  async checkUser(data) {
    let allowSendButton: boolean;
    const userData = JSON.parse(localStorage.getItem('user'));
    data.forEach((element) => {
      if (userData.name === element.creator.name) {
        allowSendButton = false;
        element.userAuthority = allowSendButton;
      } else {
        allowSendButton = true;
        element.userAuthority = allowSendButton;
      }
    });
    return data;
  }

  async presentGetErrorAlert() {
    const alert = await this.alertController.create({
      header: 'Error',
      message: '載入發生錯誤',
      buttons: ['OK'],
    });
    alert.present();
    this.allowReloadButton = false;
  }

  async notUser() {
    const alert = await this.alertController.create({
      header: '尚未登入，將引導至登入頁面',
      buttons: [
        {
          text: '確定',
          handler: () => {
            this.router.navigate(['login']);
          },
        },
      ],
    });
    alert.present();
  }

  async logOut() {
    const alert = await this.alertController.create({
      header: '確定登出嗎',
      buttons: [
        {
          text: '確定',
          handler: () => {
            this.router.navigate(['login']);
          },
        },
        {
          text: '取消',
          handler: () => {},
        },
      ],
    });
    alert.present();
  }

  newContent() {
    this.router.navigate(['new']);
  }

  editContent(id) {
    localStorage.setItem('noteID', id);
    this.router.navigate(['edit']);
  }

  async sendAlert(item) {
    const alert = await this.alertController.create({
      header: '確定傳送？',
      buttons: [
        {
          text: '確定',
          handler: () => {
            this.sendNotification(item);
          },
        },
        {
          text: '取消',
          handler: () => {},
        },
      ],
    });
    alert.present();
  }

  sendNotification(item) {
    this.data$ = this.sendByAPI(item);
  }
  sendByAPI(item) {
    const token = localStorage.getItem('token');
    const url = 'https://api.cocoing.info/admin/notifications/send';
    const body = {
      id: item.id,
    };
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.post<any>(url, body, httpOptions).pipe(
      map((response) => {
        this.presentSuccessSendAlert();
        return response.data;
      }),
      catchError(this.handleError),
    );
  }
  async presentSuccessSendAlert() {
    const alert = await this.alertController.create({
      header: 'Success',
      message: '傳送成功',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.data$ = this.initialize();
          },
        },
      ],
    });
    alert.present();
  }

  deleteItem(item) {
    this.data$ = this.deleteByAPI(item);
  }

  deleteByAPI(item) {
    const token = localStorage.getItem('token');
    const url = 'https://api.cocoing.info/admin/notifications';
    const options = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
        'X-HTTP-Method-Override': 'delete',
      }),
      body: {
        id: item.id,
      },
    };
    return this.http.delete<any>(url, options).pipe(
      map((response) => {
        this.presentSuccessAlert();
        return response.data;
      }),
      catchError(this.handleError),
    );
  }

  async presentSuccessAlert() {
    const alert = await this.alertController.create({
      header: 'Success',
      message: '刪除成功',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.data$ = this.initialize();
          },
        },
      ],
    });
    alert.present();
  }

  async presentErrorAlert() {
    const alert = await this.alertController.create({
      header: 'Error',
      message: '發生錯誤',
      buttons: ['OK'],
    });
    alert.present();
  }
  private handleError = (error: HttpErrorResponse) => {
    this.presentErrorAlert();
    // 最後的回傳值的型別應為 observable
    return throwError('Something bad happened; please try again later.');
  };
}
