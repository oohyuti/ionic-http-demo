import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
  title;
  description;
  data;
  updateTime;
  createTime;
  data$ = of(null);

  constructor(
    private router: Router,
    private http: HttpClient,
    private navController: NavController,
    private alertController: AlertController,
  ) {}

  // 初始化設定要編輯的內容
  ngOnInit() {
    this.data$ = this.initialize();
  }
  initialize() {
    const id = localStorage.getItem('noteID');
    const token = localStorage.getItem('token');
    const url = 'https://api.cocoing.info/admin/notifications/show';
    const body = {
      id: id,
    };
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
        'X-HTTP-Method-Override': 'get',
      }),
    };
    return this.http.post<any>(url, body, httpOptions).pipe(
      map((response) => {
        this.title = response.data.title;
        this.description = response.data.description;
        return response.data;
      }),
      catchError(this.getHandleError),
    );
  }
  // 偵測title變化
  changeTitle(newTitle) {
    this.title = newTitle;
  }
  // 偵測description變化
  changeDescription(newDescription) {
    this.description = newDescription;
  }

  updateContent() {
    this.data$ = this.updateByAPI();
  }
  updateByAPI() {
    const id = localStorage.getItem('noteID');
    const token = localStorage.getItem('token');
    const url = 'https://api.cocoing.info/admin/notifications';
    const body = {
      id: id,
      title: this.title,
      description: this.description,
    };
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };
    return this.http.patch<any>(url, body, httpOptions).pipe(
      map((response) => {
        this.presentSuccessAlert();
        return response.data;
      }),
      catchError(this.handleError),
    );
  }
  async presentSuccessAlert() {
    const successAlert = await this.alertController.create({
      header: 'Success',
      message: '更新成功',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.router.navigate(['/tabs/tab2']);
          },
        },
      ],
    });
    successAlert.present();
  }

  async presentErrorAlert() {
    const errorAlert = await this.alertController.create({
      header: 'Error',
      message: '更新失敗',
      buttons: ['OK'],
    });
    errorAlert.present();
  }
  async presentGetErrorAlert() {
    const errorAlert = await this.alertController.create({
      header: 'Error',
      message: '獲取失敗',
      buttons: ['OK'],
    });
    errorAlert.present();
  }

  private getHandleError = (error: HttpErrorResponse) => {
    this.presentGetErrorAlert();
    return throwError('Something bad happened; please try again later.');
  };
  private handleError = (error: HttpErrorResponse) => {
    this.presentErrorAlert();
    return throwError('Something bad happened; please try again later.');
  };
}
