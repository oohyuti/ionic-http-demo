import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  // 讓網頁的預設會跳轉到 /login 登入頁面
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full',
  },

  // 登入頁面的路由
  {
    path: 'login',
    loadChildren: () =>
      import('./login/login.module').then((m) => m.LoginPageModule),
  },

  // tabs 頁面的路由
  {
    path: '',
    loadChildren: () =>
      import('./tabs/tabs.module').then((m) => m.TabsPageModule)
  },  
  {
    path: 'edit',
    loadChildren: () =>
     import('./tab2/edit/edit.module').then( m => m.EditPageModule)
  },  
  {
    path: 'new',
    loadChildren: () =>
     import('./tab2/new/new.module').then( m => m.NewPageModule)
  },  
  {
    path: 'tab2',
    loadChildren: () =>
      import('./tab2/tab2.module').then((m) => m.Tab2PageModule)
  }, 
  {
    path: 'tab3',
    loadChildren: () =>
      import('./tab3/tab3.module').then((m) => m.Tab3PageModule)
  } 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
