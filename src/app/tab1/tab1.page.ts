import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  item;
  done;
  lists;

  //初始化陣列資料
  ngOnInit() {
    if(!localStorage.getItem("token")){
      this.notUser();
    }else{
      if(!JSON.parse(localStorage.getItem("array"))){
        this.lists = [
          {
            item:'Working',
            done:false
          },
          {
            item:'Coding',
            done:true
          }
        ];
      }else{
        this.lists=JSON.parse(localStorage.getItem("array"));
      }
    }
    }
   
  constructor(
    private navCon: NavController,
    private alertController:AlertController) { }

  addItem(itemValue){
    this.lists.unshift({item:itemValue,done:false});
    this.store();
  }
  deleteItem(itemValue){
    let index = this.lists.indexOf(itemValue);
    this.lists.splice(index, 1);
    this.store();
  }
  changeStatement(itemValue,doneValue){
    if(doneValue==false){
      this.lists.push({item:itemValue.item,done:true});
    }else{
      this.lists.unshift({item:itemValue.item,done:false});
    }
    this.deleteItem(itemValue);
    this.store();
  }
  changeItem(itemValue,changeItemValue,doneValue){
    console.log(changeItemValue);
    let index = this.lists.indexOf(itemValue);
    this.lists.splice(index, 1, {item:changeItemValue,done:doneValue});
    this.store();
  }
  store(){
    localStorage.setItem("array",JSON.stringify(this.lists));
    this.lists = JSON.parse(localStorage.getItem("array"));
  }
  backInit(){
    localStorage.clear();
    location.reload();
  }

  async logOut(){
    const alert = await this.alertController.create({
      header: '確定登出嗎',
      buttons: [
      {text:'確定',
      handler:()=>{
        this.navCon.navigateBack('login');
      }},{
      text:'取消',
      handler:()=>{}}]
    }); 
    alert.present();
}
async notUser(){
  const alert = await this.alertController.create({
    header: '尚未登入，將引導至登入頁面',
    buttons: [
    {text:'確定',
    handler:()=>{
      this.navCon.navigateBack('login');
    }}]}); 
  alert.present();
}
}
