import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {
  item;
  done;
  lists;

  //初始化陣列資料
  ngOnInit() {
    if(!JSON.parse(localStorage.getItem("array"))){
      this.lists = [
        {
          item:'Working',
          done:false
        },
        {
          item:'Coding',
          done:true
        }
      ];
    }else{
      this.lists=JSON.parse(localStorage.getItem("array"));
    }
  }
  constructor(private navCon: NavController) { }

  addItem(itemValue){
    this.lists.unshift({item:itemValue,done:false});
    this.store();
  }
  deleteItem(itemValue){
    let index = this.lists.indexOf(itemValue);
    this.lists.splice(index, 1);
    this.store();
  }
  changeStatement(itemValue,doneValue){
    if(doneValue==false){
      this.lists.push({item:itemValue.item,done:true});
    }else{
      this.lists.unshift({item:itemValue.item,done:false});
    }
    this.deleteItem(itemValue);
    this.store();
  }
  changeItem(itemValue,changeItemValue,doneValue){
    console.log(changeItemValue);
    let index = this.lists.indexOf(itemValue);
    this.lists.splice(index, 1, {item:changeItemValue,done:doneValue});
    this.store();
  }
  store(){
    localStorage.setItem("array",JSON.stringify(this.lists));
    this.lists = JSON.parse(localStorage.getItem("array"));
  }
  logout(){
    this.navCon.navigateBack('tabs/tab1');
  }
  backInit(){
    localStorage.clear();
    location.reload();
  }

}
