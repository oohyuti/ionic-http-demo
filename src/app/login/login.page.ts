import { Component } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {
  email = '';
  password = '';

  constructor(
    private http:HttpClient,
    private router:Router,
    private navController: NavController,
    private alertController: AlertController,
  ) {}

  async submit() {
    const url='https://api.cocoing.info/v1/login';
    const body={
      email:this.email,
      password:this.password
    }
    await this.http.post<any>(url, body).subscribe(
      (response) => {
        const userData={
          name:response.data.user.name,
          email:response.data.user.email
        }
        console.log(response);
        localStorage.setItem("token", response.data.token.access_token);
        localStorage.setItem("user",JSON.stringify(userData));
        this.router.navigate(['tabs/tab1']);
      },
      (error) => {
        console.error(error);
        this.presentLoginFailedAlert();
      });
  }

  async presentLoginFailedAlert() {
    const alert = await this.alertController.create({
      header: 'Wrong email or password',
      message: 'please try again',
      buttons: ['OK'],
    });

    alert.present();
  }
}
