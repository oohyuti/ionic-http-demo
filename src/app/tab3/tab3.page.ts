import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CameraResultType, Plugins } from '@capacitor/core';
import { AlertController } from '@ionic/angular';
const { Camera } = Plugins;

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss'],
})
export class Tab3Page {
  memberData;
  allow;
  imageUrl = '';
  constructor(
    private alertController: AlertController,
    private http: HttpClient,
    private router: Router,
  ) {}

  ngOnInit() {
    if (!localStorage.getItem('token')) {
      this.notUser();
    } else {
      this.initializeData();
      this.allow = true;
    }
  }
  async logOut() {
    const alert = await this.alertController.create({
      header: '確定登出嗎',
      buttons: [
        {
          text: '確定',
          handler: () => {
            this.router.navigate(['login']);
          },
        },
        {
          text: '取消',
          handler: () => {},
        },
      ],
    });
    alert.present();
  }
  async initializeData() {
    const token = localStorage.getItem('token');
    const url = 'https://api.cocoing.info/v1/login';
    const options = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${token}`,
      }),
    };
    const response = await this.http.get<any>(url, options).subscribe(
      (response) => {
        this.memberData = response.data;
        console.log(this.memberData);
        console.log(response.data);
      },
      (error) => {
        console.error(error);
        this.presentLoadFailedAlert();
      },
    );
  }

  async presentLoadFailedAlert() {
    const alert = await this.alertController.create({
      header: 'Error',
      message: 'Find no member information',
      buttons: ['OK'],
    });
    alert.present();
  }

  async notUser() {
    const alert = await this.alertController.create({
      header: '尚未登入，將引導至登入頁面',
      buttons: [
        {
          text: '確定',
          handler: () => {
            this.router.navigate(['login']);
          },
        },
      ],
    });
    alert.present();
  }

  async takePicture() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.Uri,
    });
    this.imageUrl = image.webPath;
  }
}
